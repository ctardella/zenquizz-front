var gzippo = require('gzippo');
var express = require('express');
var path = require('path');
var app = express();

app.use(gzippo.staticGzip(path.join(__dirname, '/dist')));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var port = process.env.PORT || 5000;
app.listen(port);
console.log('Magic happens on port ' + port);
