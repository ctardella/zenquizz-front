(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizzSearch', {
      templateUrl: 'app/components/quizz/search/quizzsearch.html',
      controller: function(NgTableParams, quizzservice, $filter,toastr) {
        var vm = this;
        vm.emptyMessage= true;
        vm.showMessage = false;
        vm.searchType = 'QUIZZ';

        this.search = function() {
          if(vm.nameSearched && vm.nameSearched.length){
            vm.spinner = true;
            vm.showMessage = true;
            reloadData();
          }else{
            toastr.warning('La recherche ne peut pas être vide','Attention',{timeout:1000});
          }
        };

        vm.seeQuizzFromUser = function(email){
          vm.searchType='AUTHOR';
          vm.nameSearched = email;
          vm.search();
        };

        vm.refresh = function(){
          if(vm.nameSearched && vm.nameSearched.length){
            vm.search();
          }
        }

        function loadData(){
          vm.quizzParams = new NgTableParams({}, {
            getData: function(params) {
              function handleData(data){
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                vm.spinner=false;
                if(!data.length){
                  vm.emptyMessage = true;
                }else{
                  vm.emptyMessage = false;
                }
                return data;
              }
              if(vm.searchType === 'QUIZZ'){
                  return quizzservice.getQuizzWithOwners(vm.nameSearched).then(handleData);
              }else if (vm.searchType === 'AUTHOR') {
                  return quizzservice.searchByAuthor(vm.nameSearched).then(handleData);
              }

            },
            counts: []
          });
        }

        function reloadData(){
          if(angular.isUndefined(vm.quizzParams)){
            loadData();
          }else if(vm.quizzParams !== null){
            vm.quizzParams.reload();
          }
        }

      }
    });
})();
