(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizzView', {
      templateUrl: 'app/components/quizz/view/quizz.view.html',
      bindings: {
        quizz:'<',
        heading:'<'
      }
    });
})();
