(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizzList', {
      templateUrl: 'app/components/quizz/list/quizzlist.html',
      controller: function(NgTableParams, $uibModal, $filter, quizzservice, $scope) {
        var vm = this;
        vm.spinner = true;
        this.quizzParams = new NgTableParams({}, {
          getData: function(params) {
            return quizzservice.getSummarys()
              .then(function(data) {
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                vm.spinner=false;
                if(!data.length){
                  vm.emptyMessage = true;
                }
                return data;
              });
          },
          counts: []
        });
        var quizzParams = this.quizzParams;
        this.delete = function(quizz) {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/confirm/modalList.html',
            controller: "ModalConfirmController",
            controllerAs: "vm",
            resolve: {
              datas: function() {
                return {
                  text: quizz.name,
                  type: "ce quizz"
                };
              }
            }
          });
          modalConfirm.result.then(function() {
            quizzservice.deleteOne(quizz.key).then(function() {
              $scope.$emit("refreshtree");
              quizzParams.reload();
            });
          });
        };
      },
      bindings: {}
    });
})();
