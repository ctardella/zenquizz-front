(function() {
  'use strict';

  angular
      .module('zenquizz')
      .service('quizzservice', quizzservice);

      /** @ngInject */
      function quizzservice($http,EnvironmentConfig) {
        return {
          getAll:getAll,
          createOrUpdate:createOrUpdate,
          deleteOne:deleteOne,
          get:get,
          getSummarys:getSummarys,
          getQuizzWithOwners:getQuizzWithOwners,
          fork:fork,
          getCategories:getCategories,
          deleteFromCategories:deleteFromCategories,
          renameCategory:renameCategory,
          searchByAuthor:searchByAuthor
        };

        function getAll(){
          return $http.get(EnvironmentConfig.api+'api/quizz')
          .then(get);

          function get(response){
            return response.data;
          }
        }

        function createOrUpdate(quizz){
          return $http.put(EnvironmentConfig.api+'api/quizz',quizz)
          .then(createOne);

          function createOne(response){
            return response.status;
          }
        }

        function deleteOne(id){
          return $http.delete(EnvironmentConfig.api+'api/quizz/'+id)
          .then(deleteOne);

          function deleteOne(response){
            return response.status;
          }
        }

        function get(id){
          return $http.get(EnvironmentConfig.api+'api/quizz/'+id)
          .then(getOne);

          function getOne(response){
            return response;
          }
        }


        function getSummarys() {
          return $http.get(EnvironmentConfig.api + "api/quizz/summary")
            .then(function(response) {
              return response.data;
            });
        }

        function getQuizzWithOwners(searchTerm) {
          return $http.get(EnvironmentConfig.api + 'api/quizz/owner/'+ searchTerm)
            .then(get);

          function get(response) {
            return response.data;
          }
        }

        function fork(quizz){
          return $http.put(EnvironmentConfig.api + 'api/quizz/fork/',quizz)
            .then(get);

          function get(response) {
            return response.data;
          }
        }

        function getCategories(){
          return $http.get(EnvironmentConfig.api + 'api/quizz/categories')
            .then(get);

          function get(response) {
            return response.data;
          }
        }

        function deleteFromCategories(categories){
          return $http.post(EnvironmentConfig.api + 'api/quizz/categories',categories)
            .then(get);

          function get(response) {
            return response.data;
          }
        }


        function renameCategory(categories,newName){
          return $http.put(EnvironmentConfig.api + 'api/quizz/categories/'+ newName ,categories)
            .then(get);

          function get(response) {
            return response.data;
          }
        }

        function searchByAuthor(nameSearched){
          return $http.get(EnvironmentConfig.api + 'api/quizz/search/name/'+ nameSearched)
            .then(get);

          function get(response) {
            return response.data;
          }
        }
      }
    })();
