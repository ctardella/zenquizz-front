(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizzProperties', {
      templateUrl: 'app/components/quizz/properties/quizzproperties.html',
      controller: function(quizzservice, $state, $stateParams, $scope, $cookies, $timeout,
        $uibModal, $document, moveService, $anchorScroll, $rootScope, toastr, hotkeys) {
        var vm = this;

        vm.$onInit = function() {
          vm.user = $cookies.getObject("user");
          vm.showAnswers = true;
          vm.indexesOfQuestionInError = [];

          hotkeys.add({
            combo: 'ctrl+s',
            description: 'Enregistrer le quizz',
            allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
            callback: function(event) {
              event.preventDefault();
              vm.save();
            }
          });

          var off = $rootScope.$on("$stateChangeStart", function(event, toState, toParams) {
            if (!angular.equals(vm.quizz, vm.quizzCopy)) {
              event.preventDefault();
              var modalConfirm = $uibModal.open({
                animation: true,
                templateUrl: 'app/modal/pagechange/pagechange.html',
                controller: "ModalPageChangeController",
                controllerAs: "vm"
              });
              modalConfirm.result.then(function() {
                vm.quizzCopy = vm.quizz;
                vm.save();
                updateTree();
                $state.go(toState, toParams);
              }, function() {
                vm.quizzCopy = vm.quizz;
                $state.go(toState, toParams);
              });
            }
          });

          $scope.$on('$destroy', function() {
            off();
          });
        }

        vm.save = function(userSave) {
          vm.alerts = [];
          vm.indexesOfQuestionInError = [];
          var isValid = true;
          angular.forEach(this.quizz.questions, function(question, index) {
            question.alerts = [];
            index += 1;
            isValid = isValid & checkQuestion(question, index);
          });

          if (isValid) {
            clearAlerts(this.quizz.questions);
            quizzservice.createOrUpdate(vm.quizz).
            then(function() {
                updateTree();
                if (!userSave) {
                  vm.quizzCopy = vm.quizz;
                  toastr.success('Quizz enregistré', 'Succès');
                }
              })
              .catch(function() {
                toastr.error("Nom du quizz déjà réservé pour cette catégorie","Attention");
                var modalConfirm = $uibModal.open({
                  animation: true,
                  templateUrl: 'app/modal/rename/modalRename.html',
                  controller: "ModalRenameController",
                  controllerAs: "vm",
                  resolve: {
                    datas: function() {
                      return {
                        quizz: vm.quizz
                      };
                    }
                  }
                });
                modalConfirm.result.then(function() {
                  vm.save();
                  $state.go("home.quizz", {id:vm.quizz.id});
                });
              });
          } else {
            vm.showAnswers = true;
            vm.alerts.push({
              msg: "Indices des questions en erreur " + vm.indexesOfQuestionInError,
              type: "danger"
            });
            $document.scrollTopAnimated(0, 2000);
          }
        };




        vm.delete = function() {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/confirm/modalList.html',
            controller: "ModalConfirmController",
            controllerAs: "vm",
            resolve: {
              datas: function() {
                return {
                  text: vm.quizz.name,
                  type: "ce quizz"
                };
              }
            }
          });
          modalConfirm.result.then(function() {
            quizzservice.deleteOne(vm.quizz.id).then(function() {
              updateTree();
              $state.go("home.quizzes");
            });
          });

        };

        vm.fork = function() {
          quizzservice.fork(vm.quizz).then(function(data) {
            updateTree();
            $state.go("home.quizz", {
              id: data.id
            });
          }).catch(function() {
            var modalConfirm = $uibModal.open({
              animation: true,
              templateUrl: 'app/modal/rename/modalRename.html',
              controller: "ModalRenameController",
              controllerAs: "vm",
              resolve: {
                datas: function() {
                  return {
                    quizz: vm.quizz
                  };
                }
              }
            });
            modalConfirm.result.then(function() {
              vm.fork();
            });
          });
        };


        vm.closeAlert = function(index) {
          this.alerts.splice(index, 1);
        };

        vm.isMyQuizz = function() {
          return (angular.isUndefined(this.quizz.userId) || this.quizz.userId === $cookies.getObject("user").id);
        };

        vm.addQuestion = function(index) {
          if (angular.isUndefined(this.quizz.questions)) {
            this.quizz.questions = [];
          }
          if (angular.isUndefined(index)) {
            index = 0;
          }
          this.quizz.questions.splice(index, 0, {
            fileKeys: []
          });
          vm.quizz.questions[index].answers = [];
          vm.quizz.questions[index].answers.push({});
        };

        vm.onDelete = function(question) {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/confirm/modalQuestion.html',
            controller: "ModalConfirmController",
            controllerAs: "vm",
            resolve: {
              datas: function() {
                return {
                  type: "ce quizz"
                };
              }
            }
          });
          modalConfirm.result.then(function() {
            var idx = vm.quizz.questions.indexOf(question);
            if (idx >= 0) {
              vm.quizz.questions.splice(idx, 1);
            }
          });
        };

        vm.showHideAnswers = function() {
          vm.showAnswers = !vm.showAnswers;
        }

        vm.cloneQuizz = function(){
          delete vm.quizz.id
          vm.quizz.name += "(Copie)";
          vm.quizz.userId = $cookies.getObject("user").id;
          toastr.info("Quizz Copié");
        }

        function updateTree() {
          $scope.$emit("refreshtree");
        }

        function checkQuestion(question, index) {
          var valid = true;
          switch (question.type) {
            case 'ONE_CHOICE':
              valid = checkAnswerSize(question);
              if (angular.isUndefined(question.value)) {
                question.alerts.push({
                  msg: "Aucune réponse sélectionnée.",
                  type: "danger"
                });
                vm.indexesOfQuestionInError.push(index);
                valid = false;
              }
              break;
            case 'MULTIPLE_CHOICE':
              valid = checkAnswerSize(question);
              if (question.answers.filter(isChecked).length < 1) {
                question.alerts.push({
                  msg: "Le nombre de réponses cochées doit être supérieur ou égal à 1",
                  type: "danger"
                });
                vm.indexesOfQuestionInError.push(index);
                valid = false;
              }
              break;
            case 'OPEN':
              delete question.answers;
              break;
            default:
              break;
          }
          return valid;
        }

        function checkAnswerSize(question) {
          var valid = true;
          if (angular.isUndefined(question.answers) || question.answers.length < 2) {
            question.alerts.push({
              msg: "Nombre de réponses insuffisantes",
              type: "danger"
            });
            valid = false;
          }
          return valid;
        }

        function isChecked(element) {
          return element.checked === true;
        }


        function clearAlerts(questions) {
          angular.forEach(questions, function(question) {
            question.alerts = undefined;
          });
        }

        this.moveUp = function(question) {
          var indexOfQuestion = vm.quizz.questions.indexOf(question);
          if (indexOfQuestion !== 0) {
            var nextIndex = indexOfQuestion - 1;
            moveService.move(vm.quizz.questions, indexOfQuestion, nextIndex);
            $anchorScroll('question-' + nextIndex);
          }
        };

        this.moveDown = function(question) {
          var indexOfQuestion = vm.quizz.questions.indexOf(question);
          if (indexOfQuestion.length - 1 !== indexOfQuestion) {
            var nextIndex = indexOfQuestion + 1;
            moveService.move(vm.quizz.questions, indexOfQuestion, nextIndex);
            $anchorScroll('question-' + nextIndex);
          }
        };

        this.refresh = function() {
          vm.quizzCopy = vm.quizz;
          $state.go($state.current, {}, {
            reload: true
          });
        };
      },
      bindings: {
        quizz: '=',
        categories: '=',
        quizzCopy: '='
      }
    });
})();
