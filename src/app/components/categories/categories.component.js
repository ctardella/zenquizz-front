(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('categories', {
        templateUrl: 'app/components/categories/categories.html',
        bindings:{
          values:"<"
        }
      }
  );
})();
