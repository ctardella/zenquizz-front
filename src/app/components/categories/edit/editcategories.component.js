(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('editCategories', {
        templateUrl: 'app/components/categories/edit/editcategories.html',
        controller : function(){
          var vm = this;
          this.addCategory = function() {
            if (this.currentCategory) {
              var categories = this.currentCategory.split("/");
              vm.categories =  vm.categories.concat(categories);
              this.currentCategory = "";
            }
          };

          this.deleteCategory = function(index) {
              vm.categories.splice(index, 1);
          };
        },
        bindings:{
          categories:'=',
          isMyQuizz:'<',
          edit:'<',
          existingCategories:'<',
          placeHolder:'<'
        }
      }
  );
})();
