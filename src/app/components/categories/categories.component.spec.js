(function() {
  'use strict';

  describe('Component categories', function() {

    var el;

    beforeEach(module('zenquizz'));
    beforeEach(inject(function($compile, $rootScope) {
     var scope = $rootScope.$new()
     el = angular.element('<categories values="categories"></categories>');
     $compile(el)(scope);
     scope.categories =["one","two","three"];
     $rootScope.$digest();
   }));

    it('should be compiled', function(){
      expect(el.html()).not.toEqual(null);
    });

    it('shoud contain element one',function(){
      var one = el.find("span:contains('one')");
      expect(one.length).toBe(1);
    });

  });
})();
