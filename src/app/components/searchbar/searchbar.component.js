(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('searchBar', {
      templateUrl: 'app/components/searchbar/searchbar.html',
      controller: function() {
        this.search = function() {
          this.onSearch({
            query: this.searchValue
          });
        };
      },
      bindings: {
        onSearch: '&'
      }
    });
})();
