angular
    .module('zenquizz')
    .directive('barchart', barchart);

function barchart($filter) {
    var directive = {
        link: link,
        scope:{
          chartData:'<',
          question: '<'
        },
        restrict: 'E',
        replace:false
    };
    return directive;

    function link(scope, element) {
      var data = scope.chartData;
      var question = scope.question;
      var chart = d3.select(element[0]);

      chart.append("div").attr("class", "chart")
       .selectAll('div')
       .data(data).enter()
       .append("span")
       .each(function(d,i){
         var index = i+1;
         d3.select(this).html("Réponse n°"+index);
       })
       .append("div")
       .transition()
       .ease("elastic")
       .style("width", function(d) { return d + "%"; })
       .style("background-color", function(d,i){
         if(isValidAnswer(i)){
           return '#449d44';
         }else{
           return '#d9534f';
         }
       })
       .text(function(d) { return $filter('number')(d,2) + "%"; });


       function isValidAnswer(index){
         return question.answers[index].checked || parseInt(question.value) === index;
       }
     }
}
