(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('loginpage', {
      templateUrl: 'app/components/loginpage/loginpage.html',
      controller: function(EnvironmentConfig,$sce) {
        var vm = this;
        vm.api=$sce.trustAsResourceUrl(EnvironmentConfig.api + "signin/google");
      }
    });
})();
