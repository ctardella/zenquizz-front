describe('Component: loginpage', function() {
  beforeEach(module('zenquizz'));

  var element;
  var scope;
  var controller;

  beforeEach(inject(function($rootScope,$compile,$componentController) {
    scope = $rootScope.$new();
    element = $compile(element)(scope);
    scope.$apply();
    element = angular.element('<loginpage></loginpage>');
    controller = $componentController('loginpage', {$scope: scope});
  }));

  it('should set logout url', function() {
    expect(controller.api).toBeDefined();
  });
})
