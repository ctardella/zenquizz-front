(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizzPrint', {
      templateUrl: 'app/components/print/quizz/quizz.print.html',
      controller: function(quizzservice,$stateParams){
        var vm = this;

        vm.$onInit = function(){
          vm.spinner = true;
          vm.mode = 'QUIZZ';
          vm.showSolution = false;

          quizzservice.get($stateParams.quizzId).then(function(quizz){
            vm.quizz = quizz.data;
            vm.spinner = false;
          });
        }

        vm.changeMode = function(mode){
          vm.showSolution = (mode !== 'QUIZZ');
        }
      }
    });
})();
