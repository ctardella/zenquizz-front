(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('answerPrint', {
      templateUrl: 'app/components/print/answer/answer.print.html',
      bindings:{
        answer:'=',
        index:'<',
        type:'<',
        question:'=',
        reponse:'=',
        questionIndex:'<',
        showSolution:'<'
      }
    });
})();
