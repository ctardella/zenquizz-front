(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('questionPrint', {
      templateUrl: 'app/components/print/question/question.print.html',
      bindings:{
        question:'<',
        showSolution:'<',
        index:'<'
      }
    });
})();
