(function() {
  'use strict';

  angular
      .module('zenquizz')
      .service('userservice', userservice);

      /** @ngInject */
      function userservice($http,EnvironmentConfig) {
        return {
          getCurrent:getCurrent
        };

        function getCurrent(){
          return $http.get(EnvironmentConfig.api+'api/user/current')
          .then(getCurrentComplete);

          function getCurrentComplete(response){
            return response.data;
          }
        }
      }
    })();
