(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('navbar', {
      templateUrl: 'app/components/navbar/navbar.html',
      controller: function($uibModal, gitservice) {
        this.showDetail = function() {
          gitservice.getInfos().then(function(infos) {
            var info = infos;
            $uibModal.open({
              animation: true,
              templateUrl: 'app/modal/git/modal.detail.html',
              controller: "ModalGitDetailController",
              controllerAs: "vm",
              resolve: {
                data: function() {
                  return info;
                }
              }
            });
          })
        }
      },
      bindings: {
        user: '<',
        logout: '<'
      }
    });
})();
