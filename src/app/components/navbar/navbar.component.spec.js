describe('Component: navbar', function() {
  beforeEach(module('zenquizz'));
  var element;
  var scope;
  var controller;
  var mockGitService;

  beforeEach(inject(function($rootScope, $compile, $componentController,$q) {
    var info = {};
    mockGitService = {
      getInfos: function(){
        return $q.when(info);
      }
    }
    scope = $rootScope.$new();
    element = $compile(element)(scope);
    scope.$apply();
    element = angular.element('<navbar></navbar>');
    controller = $componentController('navbar', {
      $scope: scope,
      gitservice: mockGitService
    }, {
      user: {},
      logout: {}
    });
    spyOn(mockGitService,"getInfos");
  }));

  it('should show Detail about git version', function() {
    expect(controller.showDetail).toBeDefined();
  });
})
