(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('home', {
      templateUrl: 'app/components/home/home.html',
      controller: function(userservice,$cookies,EnvironmentConfig,$sce) {
        var vm = this;
        getCurrentUser();
        vm.logout=$sce.trustAsResourceUrl(EnvironmentConfig.api + "logout");


        function getCurrentUser(){
          return userservice.getCurrent()
          .then(function(data){
              vm.user=data;
              $cookies.putObject("user",data);
              return vm.user;
          });
        }
      }
    });
})();
