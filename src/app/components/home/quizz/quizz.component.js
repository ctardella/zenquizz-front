(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('quizz', {
      templateUrl: 'app/components/home/quizz/quizz.html',
      controller: function($stateParams,quizzservice,$state,$scope) {
        var vm = this;

        vm.$onInit = function(){
          vm.quizz = {};
          vm.quizzCopy = {};
          vm.quizz.visibility = 'PRIVATE';
          vm.quizz.categories = [];
          vm.quizz.name ="";
          vm.spinner = true;

          if ($stateParams.id) {
            quizzservice.get($stateParams.id).then(function(response) {
              if (response.status === 200) {
                vm.spinner = false;
                vm.quizz = response.data;
                vm.quizzCopy = angular.copy(vm.quizz);
                $scope.$emit('$viewContentLoaded');
              } else {
                $state.go("home.quizzes");
              }
            });
          }

          if($stateParams.cat){
            vm.quizz.categories = vm.quizz.categories.concat($stateParams.cat.split(","));
          }

          quizzservice.getCategories().then(function(data){
            vm.categories = data;
          });

          vm.spinner = false;
        }
      }
    });
})();
