(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('campaign', {
      templateUrl: 'app/components/home/campaign/campaign.html',
      controller: function($stateParams, campaignservice, $state, quizzservice, $scope) {
        var vm = this;

        vm.$onInit = function(){
          vm.campaign = {};
          vm.campaign.categories = [];
          vm.campaign.recipients = [];
          vm.campaign.status = 'RUNNING';
          vm.campaign.afterSubmissionAction = 'THANK';
          vm.campaign.type = 'ANONYMOUS';
          vm.spinner = true;

          if ($stateParams.id) {
            campaignservice.get($stateParams.id).then(function(response) {
              vm.spinner = false;
              vm.campaign = response;
              vm.campaignCopy = angular.copy(vm.campaign);
              $scope.$emit('$viewContentLoaded');
            }).
            catch(function() {
              $state.go("home.campaigns");
            });
          } else {
            if ($stateParams.quizzId && $stateParams.quizzId !== "id") {
              vm.spinner = false;
              quizzservice.get($stateParams.quizzId).then(function(data) {
                var quizz = data.data;
                vm.campaign.quizzes = [];
                vm.campaign.quizzes.push(quizz);
                vm.campaign.categories = quizz.categories;
                vm.campaign.name = quizz.name;
                vm.campaign.description = quizz.description;
                vm.campaignCopy = angular.copy(vm.campaign);
              });
            }
          }

          campaignservice.getCategories().then(function(data) {
            vm.categories = data;
          });
        }
      }
    });
})();
