(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('keyValue', {
      templateUrl: 'app/components/keyvalue/keyvalue.html',
      bindings: {
        key:'<',
        value:'<'
      }
    });
})();
