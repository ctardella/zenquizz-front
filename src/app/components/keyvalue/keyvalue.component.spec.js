(function() {
  'use strict';

  describe('Component keyvalue', function() {

    var el;
    beforeEach(module('zenquizz'));
    beforeEach(inject(function($compile, $rootScope) {
      var scope = $rootScope.$new();
      el = angular.element('<keyvalue key="\'key\'" value="\'value\'"></keyvalue>');
      $compile(el)(scope);
      $rootScope.$apply();
    }));

    it('should be compiled', function() {
      expect(el.html()).not.toEqual(null);
    });

    it('shoud contain label', function() {
      var label = el.find("label:contains('key')");
      expect(label.length).toBe(0);
    });

    it('should show value', function(){
      var value = el.find("span:contains('value')");
      expect(value.length).toBe(0);
    })

    it
  });
})();
