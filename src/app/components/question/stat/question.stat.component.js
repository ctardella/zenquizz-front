(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('questionStat', {
      templateUrl: 'app/components/question/stat/question.stat.html',
      bindings: {
        questionStatistics:'<',
        index:'<'
      }
    });
})();
