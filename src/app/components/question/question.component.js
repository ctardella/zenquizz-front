(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('question', {
      templateUrl: 'app/components/question/question.html',
      controller: function(){
        if(angular.isUndefined(this.question.type)){
          this.question.type = 'MULTIPLE_CHOICE';
        }

        this.addAnswer= function () {
          if (angular.isUndefined(this.question.answers)) {
              this.question.answers =[];
          }
          this.question.answers.push({checked:false});
        };

        this.deleteAnswer = function(answer){
            var idx = this.question.answers.indexOf(answer);
            if(idx>=0){
              this.question.answers.splice(idx,1);
            }
        };

        this.fileAdd = function(key) {
          this.question.fileKeys.push(key);
        };
      },
      bindings: {
        question:'=',
        index:'=',
        onDelete:'&',
        editable:'<',
        moveUp:'&',
        moveDown:'&',
        last:'<',
        first:'<',
        showAnswers:'<',
        addQuestionHere:'&'
      }
    });
})();
