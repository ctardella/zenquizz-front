(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('questionView', {
      templateUrl: 'app/components/question/view/question.view.html',
      controller:function(){
        var vm = this;
        if(vm.response && vm.question.type === 'MULTIPLE_CHOICE'){
          vm.response.responses[vm.index] ={};
          vm.response.responses[vm.index].answers = new Array(vm.question.answers.length);
          for(var i=0;i<vm.response.responses[vm.index].answers.length;i++){
            vm.response.responses[vm.index].answers[i]=false;
          }
        }
      },
      bindings: {
        question:'=',
        index:'<',
        response:'='
      }
    });
})();
