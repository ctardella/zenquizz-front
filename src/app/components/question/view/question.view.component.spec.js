describe('Component: questionView', function() {
  beforeEach(module('zenquizz'));

  var element;
  var scope;
  var question;
  var response;

  beforeEach(inject(function($rootScope, $compile, $componentController) {
    scope = $rootScope.$new();
    element = $compile(element)(scope);
    scope.$apply();
    element = angular.element('<question-view></question-view>');
    question = {
      type: 'MULTIPLE_CHOICE',
      answers: [{}, {}, {}]
    };
    response = {
      responses: []
    };
    $componentController('questionView', {
      $scope: scope
    }, {
      question: question,
      index: 0,
      response: response
    });
  }));

  it('should set response', function() {
    expect(response.responses[0]).toBeDefined();
  });

  it('should set response size', function(){
    expect(response.responses[0].answers.length).toBe(3);
  })
})
