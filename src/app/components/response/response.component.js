(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('response', {
      templateUrl: 'app/components/response/response.html',
      controller: function($stateParams, campaignservice, $state,$filter,$document) {
        var vm = this;

        vm.$onInit = function(){
          vm.submitted = false;
          vm.alerts = [];
          vm.campaign = {};
          vm.showQuestions = true;
          vm.spinner = true;

          if ($stateParams.id) {
            campaignservice.getPublicOne($stateParams.id).then(function(data) {
                var nbQuestion = 0;
                for(var j=0;j<data.quizzes.length;j++){
                  nbQuestion += data.quizzes[j].questions.length;
                }
                vm.campaign = data;
                vm.response = {
                  responses: new Array(nbQuestion)
                };
                for(var i=0;i<nbQuestion;i++){
                  vm.response.responses[i]={};
                  vm.response.responses[i].choosenRadio = 0;
                }
                vm.questions = flatQuestions(vm.campaign.quizzes);
                vm.spinner = false;
              })
              .catch(function() {
                vm.campaign = undefined;
                vm.spinner = false;
              });
          } else {
            $state.go('main');
          }
        }

        function flatQuestions(quizzes){
          var questions = []
          for(var j=0;j< quizzes.length;j++){
            for(var i=0;i< quizzes[j].questions.length;i++){
              questions.push(quizzes[j].questions[i])
            }
          }
          return questions;
        }


        vm.hasInformation = function(){
          var infos = vm.campaign.askedRespondentInfo
          return  infos && (infos.customFields.length || infos.hasCity || infos.hasCompanyName || infos.hasLastName || infos.hasMail || infos.hasPhone)
        }

        vm.sendAnswer = function() {
          campaignservice.saveAnswer(vm.campaign.id, vm.response).then(function(data) {
            vm.submitted = true;
            switch (vm.campaign.afterSubmissionAction) {
              case 'THANK':
                vm.alerts.push({
                  msg: 'Votre réponse a bien été envoyée, merci pour votre participation.',
                  type: 'success'
                });
                vm.showQuestions = false;
                break;
              case 'SHOW_SCORE':
                vm.alerts.push({
                  msg: 'Votre réponse a été soumise, votre score est de '+ $filter('pourcent')(data.score,2) + '. Merci pour votre réponse.',
                  type: 'success'
                });
                vm.showQuestions = false;
                break;
              case 'SHOW_ANSWER':
                  vm.solution = data;
                  vm.solutionQuestion = flatQuestions(data.quizzes);
                  vm.alerts.push({
                    msg: 'Votre réponse a été soumise, votre score est de '+ $filter('pourcent')(data.quizzSubmission.score,2) + '. Merci pour votre réponse.',
                    type: 'success'
                  });
                  vm.response = undefined;
                  vm.showQuestions = false;
                  $document.scrollTopAnimated(0, 100);
                  break;
              default:
            }
          });
        };
      }
    });
})();
