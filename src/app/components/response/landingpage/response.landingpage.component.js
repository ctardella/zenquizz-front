(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('responseLandingpage', {
      templateUrl: 'app/components/response/landingpage/response.landingpage.html',
      controller: function(campaignservice,$stateParams,$document,$filter,toastr) {
        var vm = this;
        vm.alerts = [];

        campaignservice.getLandingPageInfo($stateParams.campaignId,$stateParams.recipientId).then(function(data){
            vm.infos = data;
        });

        vm.validate = function(){
          campaignservice.startResponse($stateParams.campaignId,$stateParams.recipientId).then(function(response){
                var nbQuestion = 0;
                for(var j=0;j<response.quizzes.length;j++){
                  nbQuestion += response.quizzes[j].questions.length;
                }
                vm.campaign = response;
                vm.response = {
                  responses: new Array(nbQuestion)
                };
                for(var i=0;i<nbQuestion;i++){
                  vm.response.responses[i]={};
                  vm.response.responses[i].choosenRadio = "0";
                }
                vm.questions = flatQuestions(vm.campaign.quizzes);
            }, function(){
              toastr.error('Vous ne pouvez plus répondre à ce Quizz','Erreur');
            });
        }

        function flatQuestions(quizzes){
          var questions = []
          for(var j=0;j< quizzes.length;j++){
            for(var i=0;i< quizzes[j].questions.length;i++){
              questions.push(quizzes[j].questions[i])
            }
          }
          return questions;
        }

        vm.timeout = function(){
          toastr.warning("Le temps a été dépassé, les réponses ont été soumises","Attention");
          vm.sendAnswer(true);
        }

        vm.sendAnswer = function(force) {
          var forceSubmit = angular.isDefined(force);
          campaignservice.addResponse(vm.campaign.id,$stateParams.recipientId,vm.response,forceSubmit).then(function(data) {
            vm.submitted = true;
            switch (vm.campaign.afterSubmissionAction) {
              case 'THANK':
                vm.alerts.push({
                  msg: 'Votre réponse a bien été envoyée, merci pour votre participation.',
                  type: 'success'
                });
                vm.showQuestions = false;
                break;
              case 'SHOW_SCORE':
                vm.alerts.push({
                  msg: 'Votre réponse a été soumise, votre score est de '+ $filter('pourcent')(data.score,2) + '. Merci pour votre réponse.',
                  type: 'success'
                });
                vm.showQuestions = false;
                break;
              case 'SHOW_ANSWER':
                  vm.solution = data;
                  vm.solutionQuestion = flatQuestions(data.quizzes);
                  vm.alerts.push({
                    msg: 'Votre réponse a été soumise, votre score est de '+ $filter('pourcent')(data.quizzSubmission.score,2) + '. Merci pour votre réponse.',
                    type: 'success'
                  });
                  vm.response = undefined;
                  vm.showQuestions = false;
                  $document.scrollTopAnimated(0, 100);
                  break;
              default:
            }
          });
        }

      }
    });
})();
