(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('treeMap', {
      templateUrl: 'app/components/treemap/treemap.html',
      controller: function($rootScope,treemapservice,$scope,strService) {
        var vm = this;
        vm.list = {};
        vm.spinner = true;
        refresh();

        var off = $rootScope.$on("refreshtree", function() {
          refresh();
        });

        function refresh() {
          treemapservice.getMenu().then(function(data) {
            vm.list = data;
            vm.spinner = false;
          });
        }

        vm.toggle = function(scope) {
          scope.toggle();
        };

        $scope.$on('$destroy', function() {
          off();
        });

        vm.filterNodes = function(query) {
          vm.query = query;
        };

        vm.categoryMenu = [
          ['Créer un quizz ici', function($itemScope) {
            treemapservice.createQuizz($itemScope);
          }],
          ['Supprimer les quizzes de ce dossier', function($itemScope) {
            treemapservice.deleteQuizzes($itemScope,$scope);
          }],
          ['Renommer ce dossier', function($itemScope) {
            treemapservice.renameQuizzCategory($itemScope,$scope);
          }]
        ];

        vm.headCategoryMenu = vm.categoryMenu.slice(0,2);


        vm.quizzMenu = [
          ['Supprimer ce quizz', function($itemScope) {
            treemapservice.deleteQuizz($itemScope,$scope);
          }],
          ['Créer une campagne à partir de ce quizz', function($itemScope) {
            treemapservice.createCampainFromQuizz($itemScope,$scope);
          }]
        ];

        vm.campaignCategoryMenu = [
          ['Supprimer les campagnes de ce dossier', function($itemScope) {
            treemapservice.deleteCampains($itemScope,$scope);
          }],
          ['Renommer ce dossier', function($itemScope) {
            treemapservice.renameCampaign($itemScope,$scope);
          }]
        ];

        vm.headCampaignCategoryMenu = vm.campaignCategoryMenu.slice(0,1);

        vm.campaignMenu = [
          ['Supprimer cette campagne', function($itemScope) {
            treemapservice.deleteCampain($itemScope,$scope);
          }]
        ];

        vm.visible = function(node) {
          if(angular.isUndefined(vm.query) || (vm.query && !vm.query.length) || node.name ==='Quizz' || node.name ==='Campagne'){
            return true;
          }else{
            if(!node.children.length){
              return strService.contains(node.name,vm.query) || oneCategoryValidQuery(node.categories,vm.query);
            }else{
              var childrenVisible = false;
              angular.forEach(node.children, function(value){
                childrenVisible = childrenVisible || vm.visible(value)
              });
              return childrenVisible;
            }
          }
        };

        function oneCategoryValidQuery(categories,query){
          var valid = false;
          angular.forEach(categories,function(categorie){
            valid = valid || strService.contains(categorie,query);
          });
          return valid;
        }
      }
    });
})();
