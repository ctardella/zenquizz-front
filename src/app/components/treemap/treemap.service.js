(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('treemapservice', treemapservice);

  /* @ngInject */
  function treemapservice($http, EnvironmentConfig,$state,$uibModal,quizzservice,campaignservice) {
    return {
      getMenu: getMenu,
      createQuizz:createQuizz,
      deleteQuizzes:deleteQuizzes,
      deleteQuizz:deleteQuizz,
      renameQuizzCategory:renameQuizzCategory,
      createCampainFromQuizz:createCampainFromQuizz,
      deleteCampain:deleteCampain,
      deleteCampains:deleteCampains,
      renameCampaign:renameCampaign
    };

    function getMenu() {
      return $http.get(EnvironmentConfig.api + "api/menu")
        .then(function(response) {
          return response.data;
        });
    }

    function createQuizz($itemScope){
      var categories = getParentName($itemScope);
      $state.go("home.quizz", {
        cat: categories,
        id: null
      });
    }

    function deleteQuizzes($itemScope,$scope){
      var item = $itemScope,
        quizzes = [];
      quizzes = getLeafs(item);
      var modalConfirm = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/confirm/modalList.html',
        controller: "ModalConfirmController",
        controllerAs: "vm",
        resolve: {
          datas: function() {
            return {
              text: quizzes,
              type: "ces quizzes"
            };
          }
        }
      });
      modalConfirm.result.then(function() {
        var categories = [];
        if (quizzes.length > 0) {
        categories = getParentName(item, categories);
          quizzservice.deleteFromCategories(categories).then(function() {
            $scope.$emit("refreshtree");
            $state.go("home.main", {}, {
              reload: true
            });
          });
        }
      });
    }

    function renameQuizzCategory($itemScope,$scope){
      var categories = getParentName($itemScope);
      var modalRename = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/category/modal.category.html',
        controller: 'ModalCategoryController',
        controllerAs: "vm"
      });
      modalRename.result.then(function(newName) {
        quizzservice.renameCategory(categories, newName).then(function() {
          $scope.$emit("refreshtree");
          $state.go("home.main", {}, {
            reload: true
          });
        });
      });
    }

    function deleteQuizz($itemScope,$scope){
      var modalConfirm = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/confirm/modalList.html',
        controller: "ModalConfirmController",
        controllerAs: "vm",
        resolve: {
          datas: function() {
            return {
              text: $itemScope.$modelValue.name,
              type: "ce quizz"
            };
          }
        }
      });
      modalConfirm.result.then(function() {
        quizzservice.deleteOne($itemScope.$modelValue.id).then(function() {
          $scope.$emit("refreshtree");
          $state.go("home.main", {}, {
            reload: true
          });
        });
      });
    }

    function createCampainFromQuizz($itemScope){
      $state.go("home.campaign", {
        quizzId: $itemScope.$modelValue.id,
        id: null,
        reload: true
      });
    }

    function deleteCampains($itemScope,$scope){
      var campaigns = getCompleteLeafs($itemScope);
      var modalConfirm = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/campaign/delete.html',
        controller: 'ModalDeleteCampaignController',
        controllerAs: "vm",
        resolve: {
          datas: function() {
            return campaigns;
          }
        }
      });
      modalConfirm.result.then(function() {
        var categories = [];
        if (campaigns.length > 0) {
          categories = getParentName($itemScope, categories);
          campaignservice.deleteFromCategories(categories).then(function() {
            $scope.$emit("refreshtree");
            $state.go("home.main", {}, {
              reload: true
            });
          });
        }
      });
    }

    function renameCampaign($itemScope,$scope){
      var categories = getParentName($itemScope);
      var modalRename = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/category/modal.category.html',
        controller: 'ModalCategoryController',
        controllerAs: "vm"
      });
      modalRename.result.then(function(newName) {
        campaignservice.renameCategory(categories, newName).then(function() {
          $scope.$emit("refreshtree");
          $state.go("home.main", {}, {
            reload: true
          });
        });
      });
    }

    function deleteCampain($itemScope,$scope){
      var modalConfirm = $uibModal.open({
        animation: true,
        templateUrl: 'app/modal/confirm/modalList.html',
        controller: "ModalConfirmController",
        controllerAs: "vm",
        resolve: {
          datas: function() {
            return {
              text: $itemScope.$modelValue.name,
              type: "cette campagne"
            };
          }
        }
      });
      modalConfirm.result.then(function() {
        campaignservice.deleteOne($itemScope.$modelValue.id).then(function() {
          $scope.$emit("refreshtree");
          $state.go("home.main", {}, {
            reload: true
          });
        });
      });
    }


    function getLeafs(item) {
      var items = [];
      if (!item.hasChild() && angular.isDefined(item.$modelValue)) {
        items.push(item.$modelValue.name);
      } else {
        var childNodes = item.childNodes();
        for (var i = 0; i < childNodes.length; i++) {
          items = items.concat(getLeafs(childNodes[i]));
        }
      }
      return items;
    }

    function getCompleteLeafs(item){
      var items = [];
      if (!item.hasChild() && angular.isDefined(item.$modelValue)) {
        items.push(item.$modelValue);
      } else {
        var childNodes = item.childNodes();
        for (var i = 0; i < childNodes.length; i++) {
          items = items.concat(getCompleteLeafs(childNodes[i]));
        }
      }
      return items;
    }

    function getParentName(item) {
      var categories = [];
      while (angular.isDefined(item.$parentNodeScope)) {
        if (angular.isDefined(item.$modelValue)) {
          categories.push(item.$modelValue.name);
          item = item.$parentNodeScope;
        } else {
          item = {};
        }
      }
      return categories.reverse();
    }

  }
})();
