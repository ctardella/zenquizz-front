(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('campaignList', {
      templateUrl: 'app/components/campaign/list/campaignlist.html',
      controller: function(NgTableParams, campaignservice, $filter, $uibModal, $scope) {
        var vm = this;
        vm.spinner = true;
        this.campaignParams = new NgTableParams({}, {
          getData: function(params) {
            return campaignservice.getSummarys()
              .then(function(data) {
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                vm.spinner = false;
                if (!data.length) {
                  vm.emptyMessage = true;
                }
                return data;
              });
          },
          counts: []
        });

        vm.deleteCampaign = function(campaign) {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/confirm/modalList.html',
            controller: "ModalConfirmController",
            controllerAs: "vm",
            resolve: {
              datas: function() {
                return {
                  text: campaign.name,
                  type: "cette campagne"
                };
              }
            }
          });
          modalConfirm.result.then(function() {
            campaignservice.deleteOne(campaign.key).then(function() {
              $scope.$emit("refreshtree");
              vm.campaignParams.reload();
            });
          });
        };

        campaignservice.countArchivedCampaigns().then(function(nb){
          if(nb>0){
            vm.showArchivedLink = true;
          }
        });
      }
    });
})();
