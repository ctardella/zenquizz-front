(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('campaignCreate', {
        templateUrl: 'app/components/campaign/create/campaign.create.html',
        controller: function(quizzservice){
          var vm = this;

          quizzservice.getSummarys().then(function(data){
            if(data.length === 0){
              vm.alert = {
                msg:"Aucun quizz disponible.",
                type:"danger"
              };
            }
            vm.quizzes = data;
          });
        }
      }
  );
})();
