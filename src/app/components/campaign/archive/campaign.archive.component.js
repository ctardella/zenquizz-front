(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('campaignArchive', {
        templateUrl: 'app/components/campaign/archive/campaign.archive.html',
        controller: function(campaignservice,$filter,$scope,NgTableParams,$uibModal){
          var vm = this;
          vm.spinner = true;
          this.campaignParams = new NgTableParams({}, {
            getData: function(params) {
              return campaignservice.getArchivedCampaign()
                .then(function(data) {
                  data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                  vm.spinner = false;
                  if (!data.length) {
                    vm.emptyMessage = true;
                  }
                  return data;
                });
            },
            counts: []
          });

          vm.deleteCampaign = function(campaign) {
            var modalConfirm = $uibModal.open({
              animation: true,
              templateUrl: 'app/modal/confirm/modalList.html',
              controller: "ModalConfirmController",
              controllerAs: "vm",
              resolve: {
                datas: function() {
                  return {
                    text: campaign.name,
                    type: "cette campagne"
                  };
                }
              }
            });
            modalConfirm.result.then(function() {
              campaignservice.deleteOne(campaign.id).then(function() {
                $scope.$emit("refreshtree");
                vm.campaignParams.reload();
              });
            });
          };
        }
      }
  );
})();
