(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('campaignservice', campaignservice);

  /** @ngInject */
  function campaignservice($http, EnvironmentConfig) {
    return {
      getSummarys: getSummarys,
      get: getOne,
      createOrUpdate: createOrUpdate,
      deleteOne: deleteOne,
      getCategories: getCategories,
      getPublicOne: getPublicOne,
      saveAnswer: saveAnswer,
      deleteFromCategories: deleteFromCategories,
      renameCategory: renameCategory,
      countArchivedCampaigns:countArchivedCampaigns,
      archiveCampaign:archiveCampaign,
      getArchivedCampaign:getArchivedCampaign,
      getLandingPageInfo:getLandingPageInfo,
      startResponse:startResponse,
      addResponse:addResponse,
      addQuizz:addQuizz
    };

    function getSummarys() {
      return $http.get(EnvironmentConfig.api + 'api/campaign/summary')
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function getOne(id) {
      return $http.get(EnvironmentConfig.api + 'api/campaign/' + id)
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function createOrUpdate(campaign) {
      return $http.put(EnvironmentConfig.api + 'api/campaign/', campaign)
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function deleteOne(id) {
      return $http.delete(EnvironmentConfig.api + 'api/campaign/' + id)
        .then(del);

      function del(response) {
        return response;
      }
    }

    function deleteFromCategories(categories) {
      return $http.post(EnvironmentConfig.api + 'api/campaign/categories', categories)
        .then(del);

      function del(response) {
        return response;
      }
    }

    function getCategories() {
      return $http.get(EnvironmentConfig.api + 'api/campaign/categories')
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function getPublicOne(id) {
      return $http.get(EnvironmentConfig.api + 'public/api/campaign/' + id)
        .then(get)
        .catch(function(error) {
          return error;
        });

      function get(response) {
        return response.data;
      }
    }

    function saveAnswer(campaignId, response) {
      return $http.post(EnvironmentConfig.api + 'public/api/campaign/' + campaignId, response)
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function renameCategory(categories, newName) {
      return $http.put(EnvironmentConfig.api + 'api/campaign/categories/' + newName, categories)
        .then(get);

      function get(response) {
        return response.data;
      }
    }

    function countArchivedCampaigns() {
      return $http.get(EnvironmentConfig.api + 'api/campaign/archived/count')
        .then(function (response){
          return response.data
        });
    }

    function archiveCampaign(campaignId) {
      return $http.put(EnvironmentConfig.api + 'api/campaign/archived/' + campaignId)
      .then( function (response){
        return response;
      })
    }

    function getArchivedCampaign(){
      return $http.get(EnvironmentConfig.api + 'api/campaign/archived')
      .then( function (response){
        return response.data;
      })
    }

    function getLandingPageInfo(campaignId,recipientId){
      return $http.get(EnvironmentConfig.api + 'public/api/campaign/landingpage/'+ campaignId + '/' + recipientId)
      .then( function (response){
        return response.data;
      })
    }

    function startResponse(campaignId,recipientId){
      return $http.put(EnvironmentConfig.api + 'public/api/campaign/'+ campaignId + '/' + recipientId)
      .then( function (response){
        return response.data;
      })
    }

    function addResponse(campaignId,recipientId,response,force){
      return $http.post(EnvironmentConfig.api + 'public/api/campaign/'+ campaignId + '/' + recipientId , response,{params:{ force : force }})
      .then( function (response){
        return response.data;
      })
    }

    function addQuizz(campaignId,quizzId){
      return $http.put(EnvironmentConfig.api + 'api/campaign/addQuizz/'+ campaignId + '/' + quizzId)
      .then( function (response){
        return response.data;
      })
    }
  }
})();
