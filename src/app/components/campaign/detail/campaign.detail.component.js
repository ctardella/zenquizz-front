(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('campaignDetail', {
        templateUrl: 'app/components/campaign/detail/campaign.detail.html',
        controller:function ($stateParams,campaignStatisticsService) {
          var vm=this;
          vm.campaignId = $stateParams.id;


          campaignStatisticsService.getCampaignStatisticsByQuestion(vm.campaignId).then(function(data){
            vm.questionStatistics = data;
          })

          campaignStatisticsService.getQuizzSubmissionByCampaign(vm.campaignId).then(function(data){
            vm.quizzSubmissions = data;
            vm.meanScore = meanScore(data);
          })

          function meanScore(quizzSubmissions){
            var mean = 0;
            angular.forEach(quizzSubmissions,function(quizzSubmission){
              mean +=quizzSubmission.score;
            })
            return mean = mean / quizzSubmissions.length;
          }
        }
      }
  );
})();
