(function() {
  'use strict';

  angular
      .module('zenquizz')
      .service('campaignStatisticsService', campaignStatisticsService);

      /** @ngInject */
      function campaignStatisticsService($http,EnvironmentConfig) {
        return {
          getCampaignStatisticsByQuestion:getCampaignStatisticsByQuestion,
          getQuizzSubmissionByCampaign:getQuizzSubmissionByCampaign
        };

        function getCampaignStatisticsByQuestion(campaignId){
          return $http.get(EnvironmentConfig.api+'api/statistics/campaign/question/'+campaignId)
          .then(get);

          function get(response){
            return response.data;
          }
        }

        function getQuizzSubmissionByCampaign(campaignId){
          return $http.get(EnvironmentConfig.api+'api/statistics/campaign/response/'+campaignId)
          .then(get);

          function get(response){
            return response.data;
          }
        }
      }
    })();
