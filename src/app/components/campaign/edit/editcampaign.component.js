(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('editCampaign', {
      templateUrl: 'app/components/campaign/edit/editcampaign.html',
      controller: function(campaignservice, $state, $scope, $timeout, $uibModal, mailservice, toastr, $rootScope, hotkeys,quizzservice) {
        var vm = this;

        vm.$onInit = function() {
          vm.open = {};
          vm.open.startDate = false;
          vm.open.endDate = false;
          vm.mailToSend = [];

          hotkeys.add({
            combo: 'ctrl+s',
            description: 'Enregistrer la campagne',
            allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
            callback: function(event) {
              event.preventDefault();
              vm.save();
            }
          });

          var off = $rootScope.$on("$stateChangeStart", function(event, toState, toParams) {
            if (!angular.equals(vm.campaign, vm.campaignCopy)) {
              event.preventDefault();
              var modalConfirm = $uibModal.open({
                animation: true,
                templateUrl: 'app/modal/pagechange/pagechange.html',
                controller: "ModalPageChangeController",
                controllerAs: "vm"
              });
              modalConfirm.result.then(function() {
                vm.campaignCopy = vm.campaign;
                vm.save();
                $state.go(toState, toParams);
              }, function() {
                vm.campaignCopy = vm.campaign;
                $state.go(toState, toParams);
              });
            }
          });

          summarys();

          $scope.$on('$destroy', function() {
            off();
          });
        }

        vm.changeType =  function(){
          if(vm.campaign.type !== 'ANONYMOUS'){
            vm.campaign.status = 'RUNNING';
          }
        }

        this.open = function(popin) {
          popin.opened = true;
        };


        vm.answerPage = function() {
          return $state.href('answer', {
            id: vm.campaign.id
          }, {
            absolute: true
          });
        }

        vm.save = function() {
          delete vm.campaign.open;
          if (vm.mailToSend.length > 0 && !vm.modalSaveHasBeenShown) {
            saveMailPopin(function() {
              return saveCampaign(vm.campaign, true);
            });
          } else {
            return saveCampaign(vm.campaign, false);
          }
        };

        function saveCampaign(campaign, saveMail) {
          return campaignservice.createOrUpdate(campaign)
            .then(function(data) {
              $scope.$emit("refreshtree");
              vm.campaign = data;
              updateCopy(vm.campaign)
              if (saveMail) {
                campaign.id = data.id;
                vm.sendMail();
                return true;
              }
              toastr.success('Campagne enregistrée', 'Succès');
              return true;
            })
            .catch(function() {
              toastr.error("Nom de la campagne indisponible dans ce dossier", 'Une erreur est survenue', {
                timeout: 3000
              });
              return false;
            });
        }

        vm.delete = function() {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/confirm/modalList.html',
            controller: "ModalConfirmController",
            controllerAs: "vm",
            resolve: {
              datas: function() {
                return {
                  text: vm.campaign.name,
                  type: "cette campagne"
                };
              }
            }
          });

          modalConfirm.result.then(function() {
            campaignservice.deleteOne(vm.campaign.id).then(function() {
              $scope.$emit("refreshtree");
              updateCopy(vm.campaign)
              $state.go("home.campaigns");
            });
          });
        };

        vm.openCalendar = function(e, date) {
          vm.open[date] = true;
        };


        vm.sendMail = function() {
          if (vm.mailToSend.length > 0) {
            if (vm.campaign.id) {
              mailservice.sendMailFormCampaign(vm.campaign.id, vm.mailToSend)
                .then(function(data) {
                  vm.campaign.recipients = vm.campaign.recipients.concat(data);
                  vm.mailToSend = [];
                  toastr.success('Mail(s) envoyé(s)', 'Succès')
                })
                .catch(function(value) {
                  toastr.error(value, 'Une erreur est survenue', {
                    timeout: 1000
                  });
                });
            }
          } else {
            toastr.warning('Aucun mail à envoyer.', 'Attention', {
              timeout: 1000
            });
          }
        };

        vm.clone = function(){
          delete vm.campaign.id;
          vm.campaign.name += '(Copie)';
        }

        vm.archive = function(){
          vm.campaign.archived = !vm.campaign.archived
          campaignservice.archiveCampaign(vm.campaign.id).then(function(){
            var msg;
            if(vm.campaign.archived){
              msg = 'Campagne archivée';
            }else{
              msg = 'Campagne Désarchivée';
            }
            updateCopy(vm.campaign)
            toastr.success(msg,'Succès');
            $scope.$emit("refreshtree");
          });
        }

        vm.addQuizz = function(quizzId){
          campaignservice.addQuizz(vm.campaign.id,quizzId).then(function(campaign){
            updateCopy(campaign)
            vm.campaign = campaign;
          });
        }

        function updateCopy(campaign){
          vm.campaignCopy = campaign;
        }

        function summarys() {
          quizzservice.getSummarys().then(function(data){
            if(data.length === 0){
              vm.addAlert = {
                msg:"Aucun quizz disponible.",
                type:"danger"
              };
            }
            vm.quizzes = data;
          });
        }

        function saveMailPopin(save) {
          var modalConfirm = $uibModal.open({
            animation: true,
            templateUrl: 'app/modal/mail/modal.mail.save.html',
            controller: "ModalMailController",
            controllerAs: "vm"
          });
          modalConfirm.result.then(save);
        }


      },
      bindings: {
        campaign: '=',
        categories: '<',
        campaignCopy: '='
      }
    });
})();
