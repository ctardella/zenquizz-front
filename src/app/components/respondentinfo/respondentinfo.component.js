(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('respondentInfo', {
        templateUrl: 'app/components/respondentinfo/respondentinfo.html',
        controller:function(){
          var vm=this;

          if(angular.isUndefined(vm.infos)){
            vm.infos ={};
            vm.infos.customFields =[];
          }

          vm.addField = function(){
            if(angular.isDefined(vm.name.length) && vm.infos.customFields.indexOf(vm.name)===-1){
              vm.infos.customFields.push(vm.name);
              vm.name = undefined;
            }
          };

          vm.delete = function (index) {
            vm.infos.customFields.splice(index,1);
          };
        },
        bindings:{
          infos:"=",
          showUsual:'<'
        }
      }
  );
})();
