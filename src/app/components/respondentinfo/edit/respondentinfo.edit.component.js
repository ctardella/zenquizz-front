(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('respondentInfoEdit', {
      templateUrl: 'app/components/respondentinfo/edit/respondentinfo.edit.html',
      controller: function() {
        var vm = this;
        vm.customFields = {};
        angular.forEach(vm.infos.customFields, function(value) {
          vm.customFields[value]="";
        });
        vm.infoValues = {};
        vm.infoValues.customFields = vm.customFields;
      },
      bindings: {
        infos: "<",
        infoValues: "=",
        valid:"="
      }
    });
})();
