(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('respondentInfoList', {
      templateUrl: 'app/components/respondentinfo/list/respondentinfo.list.html',
      controller:function(){
        var vm = this;
        vm.isLow = function(score){
          return score < 0.5;
        }
        vm.isWarning = function(score){
          return score>= 0.5 && score<0.65;
        }
        vm.isQuiteGood = function(score){
          return score>= 0.65 && score<0.8;
        }
        vm.isExcelent = function(score){
          return score>0.8;
        }
      },
      bindings: {
        quizzSubmissions:'<'
      }
    });
})();
