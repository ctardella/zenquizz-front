(function() {
  'use strict';

  angular
      .module('zenquizz')
      .service('mailservice', mailservice);

      /** @ngInject */
      function mailservice($http,EnvironmentConfig) {
        return {
          sendMailFormCampaign:sendMailFormCampaign
        };

        function sendMailFormCampaign(campaignId,mails){
          return $http.post(EnvironmentConfig.api + 'api/mail/campaign/'+ campaignId,mails)
            .then(get)
            .catch(get);

          function get(response) {
            return response.data;
          }
        }
      }
    })();
