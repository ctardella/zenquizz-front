describe('Component: mailList', function() {
  beforeEach(module('zenquizz'));

  var element;
  var scope;
  var controller;
  var sendMail;

  beforeEach(inject(function($rootScope,$compile,$componentController) {
    scope = $rootScope.$new();
    element = $compile(element)(scope);
    scope.$apply();
    element = angular.element('<mail-list></mail-list>');
    sendMail = jasmine.createSpy();
    controller = $componentController('mailList', {$scope: scope}, {mails:[],mailToSend:[],sendMail:sendMail});
  }));

  it('should add mailToSend to mails', function() {
    controller.mail = 'test@test.com';
    controller.addMail();
    expect(controller.mailToSend.length).toBe(1);
    expect(controller.mail).toBe('');
  });

  it('should delete mail in list',function(){
    controller.mail = 'test@test.com';
    controller.addMail();
    controller.deleteMail(0);
    expect(controller.mailToSend.length).toBe(0);
  });
})
