(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('mailList', {
      templateUrl: 'app/components/maillist/maillist.html',
      controller: function() {
        var vm = this;


        this.addMail = function() {
          if (vm.mail && vm.mail.gender && vm.mail.mail && vm.mail.firstName && vm.mail.lastName) {
            vm.mailToSend.push(vm.mail);
            vm.mail = {};
          }
        };

        this.deleteMail = function(index) {
          vm.mailToSend.splice(index, 1);
        };
      },
      bindings: {
        mails: '=',
        mailToSend:'=',
        sendMail:'&'
      }
    });
})();
