(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('fileUpload', {
      templateUrl: 'app/components/fileUpload/fileUpload.html',
      controller: function(fileUploadService, clipboard, toastr, $filter, mimetype, FileUpload,EnvironmentConfig) {
        var vm = this;
        vm.nbFileInUploadQueue = 0;
        vm.baseFileUrl = EnvironmentConfig.api + "public/file/";

        this.fileAdded = function($file) {
          if ($file.size < FileUpload.MAX_SIZE) {
            vm.nbFileInUploadQueue++;
            var formData = new FormData();
            formData.append('file', $file.file);
            fileUploadService.saveFile(formData).then(function(file) {
              vm.fileAdd({
                key: file.key
              });
              vm.nbFileInUploadQueue--;
            });
          } else {
            toastr.error('La taille du fichier ' + $file.name + ' est trop importante.', 'Attention', {
              timeOut: 2000
            });
          }
        };

        this.deleteFile = function(index) {
          vm.files.splice(index, 1);
        };

        vm.copyImgInsertCode = function(index) {
          copyToClipBoard('![' + $filter('attachedFile')(vm.files[index]) + '](' + EnvironmentConfig.api + "public/file/" + vm.files[index] + ')');
        }

        vm.copyFileInsertCode = function(index) {
          copyToClipBoard('<a href=\"' + EnvironmentConfig.api + "public/file/"+  vm.files[index] + '\" target="_blank">' + $filter('attachedFile')(vm.files[index]) + '</a>');
        }

        function copyToClipBoard(text) {
          clipboard.copyText(text);
          toastr.success('collez-le dans une zone Markdown', 'Code d\'insertion copié', {
            timeOut: 1000
          });
        }

        this.isImg = function(url) {
          return mimetype.isImg($filter('extension')(url));
        }
      },
      bindings: {
        files: '=',
        fileAdd: '&'
      }
    });
})();
