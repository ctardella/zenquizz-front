(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('fileUploadService', fileUploadService);

  /* @ngInject */
  function fileUploadService($http, EnvironmentConfig) {
    return {
      saveFile: saveFile
    };

    function saveFile(formData) {
      return $http({
        method: "POST",
        url: EnvironmentConfig.api + 'api/file',
        headers: {
          'Content-Type': undefined
        },
        data:formData
      }).then(function(response) {
        return response.data;
      });
    }
  }
})();
