(function() {
  'use strict';

  describe('Component breadcrumb', function() {

    var el;

    beforeEach(module('zenquizz'));
    beforeEach(inject(function($compile, $rootScope) {
    var scope = $rootScope.$new()
     el = angular.element('<breadcrumb></breadcrumb>');
     $compile(el)(scope);
     $rootScope.$digest();
   }));

    it('should be compiled', function(){
      expect(el.html()).not.toEqual(null);
    });

    it('shoud contain directive ncy-breadcrumb',function(){
      var div = el.find('div[ncy-breadcrumb]');
      expect(div).toBeDefined();
    });

  });
})();
