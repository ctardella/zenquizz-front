(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('breadcrumb', {
        templateUrl: 'app/components/breadcrumb/breadcrumb.html'
      }
  );
})();
