(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('answer', {
      templateUrl: 'app/components/answer/answer.html',
      controller: function() {
        var vm = this;

        vm.$onInit = function(){
          vm.tooltip = "Ajouter un commentaire";
          vm.commentVisible = false;
          if (vm.answer.comment) {
            vm.tooltip = "Supprimer le commentaire";
            vm.commentVisible = true;
          }
        }

        vm.showComment = function() {
          if (vm.commentVisible) {
            vm.tooltip = "Ajouter un commentaire";
            vm.commentVisible = false;
          } else {
            vm.tooltip = "Supprimer le commentaire";
            vm.answer.comment = undefined;
            vm.commentVisible = true;
          }
        };
      },
      bindings: {
        answer: '=',
        answerIndex: '<',
        index: '=',
        onDelete: '&',
        editable: '<',
        type: '<',
        question: '='
      }
    });
})();
