(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('answerView', {
      templateUrl: 'app/components/answer/view/answer.view.html',
      bindings: {
        answer:'=',
        index:'<',
        type:'<',
        question:'=',
        reponse:'=',
        questionIndex:'<'
      }
    });
})();
