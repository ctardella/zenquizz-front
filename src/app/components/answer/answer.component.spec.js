describe('Component: answer', function() {
  beforeEach(module('zenquizz'));

  var element;
  var scope;
  var answer;
  var controller;

  beforeEach(inject(function($rootScope, $compile, $componentController) {
    scope = $rootScope.$new();
    element = $compile(element)(scope);
    scope.$apply();
    element = angular.element('<answer></answer>');
    answer = {};
    controller = $componentController('answer', {
      $scope: scope
    }, {
      answer:answer
    });
  }));

  it('should show comments', function() {
    expect(controller.showComment).toBeDefined();
    controller.showComment();
    scope.commentVisible = true;
    controller.showComment();
  });
  
})
