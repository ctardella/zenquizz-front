(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('countdown', {
      templateUrl: 'app/components/countdown/countdown.html',
      controller: function($interval) {
        var vm = this;

        vm.delay =  {
          minutes:vm.minutes,
          secondes:0
        }
        vm.interval = $interval(function(){
          if(vm.delay.secondes == 0){
            vm.delay.minutes--;
            vm.delay.secondes = 59;
          }else{
            vm.delay.secondes--;
          }
          if(vm.delay.minutes == 0 && vm.delay.secondes == 0){
            vm.stopInterval();
            vm.end();
          }
        },1000)

        vm.stopInterval = function(){
          $interval.cancel(vm.interval)
        }

        vm.$onDestroy = function(){
          vm.stopInterval();
        }
      },
      bindings: {
        minutes: '<',
        pause: '<',
        end:'&'
      }
    });
})();
