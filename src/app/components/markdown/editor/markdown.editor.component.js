(function() {
  'use strict';
  angular
    .module('zenquizz')
    .component('markdownEditor', {
        templateUrl: 'app/components/markdown/editor/markdown.editor.html',
        bindings: {
          text:'=',
          required:'<',
          placeHolder:'<',
          headText:'<'
        }
      }
  );
})();
