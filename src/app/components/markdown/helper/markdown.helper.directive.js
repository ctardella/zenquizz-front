(function() {
    'use strict';

    angular
        .module('zenquizz')
        .directive('markdownHelper', markdownHelper);

    /* @ngInject */
    function markdownHelper() {
        return {
            restrict: 'EA',
            templateUrl: 'app/components/markdown/helper/markdown.helper.html'
        };
    }
})();
