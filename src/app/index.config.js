(function() {
  'use strict';

  angular
    .module('zenquizz')
    .config(config);

  /** @ngInject */
  function config($httpProvider, $logProvider, usSpinnerConfigProvider, markedProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('responseObserver');
    usSpinnerConfigProvider.setDefaults({
      color: '#af1e3a'
    });

    var $log = angular.injector(['ng']).get('$log');

    markedProvider.setOptions({
      gfm: true,
      tables: true,
      highlight: function(code, lang) {
        if (lang) {
          try{
            return hljs.highlight(lang, code, true).value;
          }catch(ex){
            $log.info(ex);
          }
        } else {
          return hljs.highlightAuto(code).value;
        }
      }
    });
  }

})();
