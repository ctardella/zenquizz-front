(function() {
  'use strict';

  angular
    .module('zenquizz')
    .factory('responseObserver',function($q, $location) {
      return {
        'responseError': function(errorResponse) {
            switch (errorResponse.status) {
            case 403:
                $location.path('/main');
                break;
            }
            return $q.reject(errorResponse);
        }
    };
    });
})();
