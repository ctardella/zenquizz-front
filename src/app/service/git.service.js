(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('gitservice', gitservice);

  /* @ngInject */
  function gitservice($http,EnvironmentConfig) {
    return {
      getInfos: getInfos
    };

    function getInfos() {
      return $http.get(EnvironmentConfig.api+'actuator/info')
      .then(get);

      function get(response){
        return response.data;
      }
    }
  }
})();
