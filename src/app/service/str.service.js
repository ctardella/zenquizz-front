(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('strService', strService);

  /* @ngInject */
  function strService($filter) {
    return {
      compare: compare,
      contains:contains
    };

    function compare(str1,str2) {
      var str1U  = ($filter('diacritics')(str1)).toUpperCase(),
      str2U = ($filter('diacritics')(str2)).toUpperCase();
      return str1U === str2U;
    }

    function contains(str1,str2){
      var str1U = ($filter('diacritics')(str1)).toUpperCase(),
      str2U = ($filter('diacritics')(str2)).toUpperCase();
      return str1U.indexOf(str2U)!=-1
    }
  }
})();
