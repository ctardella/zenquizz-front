(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('mimetype', mimetype);

  /* @ngInject */
  function mimetype() {
    return {
      isImg: isImg
    };

    function isImg(extension) {
      var knowExtensions = ["tif","tiff","gif","jpeg","jpg","jif","jfif","jp2","jpx","j2k","j2c","fpx","pcd","pcd","png","pdf","svg","bmp"];
      return knowExtensions.indexOf(extension.toLowerCase())!=-1;
    }
  }
})();
