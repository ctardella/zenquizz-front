(function() {
  'use strict';

  angular
    .module('zenquizz')
    .service('moveService', moveService);

  /* @ngInject */
  function moveService() {
    return {
      move: move
    };

    function move(array, indexFrom, indexTo) {
      array.splice(indexTo, 0, array.splice(indexFrom, 1)[0]);
    }
  }
})();
