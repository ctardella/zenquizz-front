(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('campaignStatus', campaignStatus);

  function campaignStatus() {
    return campaignStatusFilter;

    function campaignStatusFilter(text) {
      var campaignStatus = {
        RUNNING:"Ouverte",
        CLOSED:"Fermée",
        CONDITIONED:"Conditionée",
        ARCHIVED:"Archivée"
      };
      return campaignStatus[text] || text;
    }
  }
})();
