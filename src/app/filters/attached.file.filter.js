(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('attachedFile', attachedFile);

  function attachedFile() {
    return function(key) {
      if (angular.isDefined(key)) {
        var UUID_SIZE = 37;
        return key.substr(UUID_SIZE);
      }
    };
  }
})();
