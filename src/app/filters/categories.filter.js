(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('categories', categories);

  function categories() {
    return categoriesFilter;

    function categoriesFilter(cat) {
      if (angular.isDefined(cat)) {
        var str = "";
        for (var i = 0; i < cat.length; i++) {
          str += cat[i];
          if(i !== cat.length -1){
            str+=" / ";
          }
        }
        if(cat.length > 0){
          str = '('+ str;
          str +=')';
        }
        return str;
      }
    }
  }
})();
