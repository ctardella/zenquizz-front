(function() {
  'use strict';

  describe('Filter visibility', function() {


    beforeEach(module('zenquizz'));


    it('should has a visibility filter', inject(function($filter){
      expect($filter('visibility')).not.toEqual(null);
    }));

    it('should return text value',inject(function(visibilityFilter){
      expect(visibilityFilter("test")).toBe("test");
    }));

    it('should return a readble string',inject(function(visibilityFilter){
      expect(visibilityFilter("PUBLIC")).toBe("publique");
      expect(visibilityFilter("PRIVATE")).toBe("privée");
    }));
  });
})();
