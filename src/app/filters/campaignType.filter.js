(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('campaignType', campaignType);

  function campaignType() {
    return campaignTypeFilter;

    function campaignTypeFilter(text) {
      var campaignType = {
        ANONYMOUS:"Anonyme",
        INDIVIDUALIZED:"Individuelle"
      };
      return campaignType[text] || text;
    }
  }
})();
