(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('visibility', visibility);

  function visibility() {
    return visibilityFilter;

    function visibilityFilter(text) {
      var visibility = {
        PUBLIC:"publique",
        PRIVATE:"privée"
      };
      return visibility[text] || text;
    }
  }
})();
