(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('pourcent', pourcent);

  function pourcent($filter) {
    return pourcentFilter;

    function pourcentFilter(input, decimals) {
      return $filter('number')(input * 100, decimals) + '%';
    }
  }
})();
