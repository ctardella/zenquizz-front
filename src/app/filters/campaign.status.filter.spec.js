(function() {
  'use strict';

  describe('Filter Campaign Status', function() {


    beforeEach(module('zenquizz'));


    it('should has a Campaign Status', inject(function($filter){
      expect($filter('campaignStatus')).not.toEqual(null);
    }));

    it('should return text', inject(function(campaignStatusFilter){
      expect(campaignStatusFilter("test")).toBe("test");
    }));

    it('should return "ouverte"',inject(function(campaignStatusFilter){
      expect(campaignStatusFilter("RUNNING")).toBe("Ouverte");
    }));
  });
})();
