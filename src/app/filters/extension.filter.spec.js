(function() {
  'use strict';

  describe('Filter extension', function() {


    beforeEach(module('zenquizz'));


    it('should has an extension filter', function($filter){
      expect($filter('extension')).not.toEqual(null);
    });

    it('should do nothing', inject(function(extensionFilter){
      expect(extensionFilter()).toBeUndefined();
    }));

    it('should extract extension from file name',inject(function(extensionFilter){
      expect(extensionFilter("test.js")).toBe("js");
      expect(extensionFilter("test.java.js")).toBe("js");
    }));
  });
})();
