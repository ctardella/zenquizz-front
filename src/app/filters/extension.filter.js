(function() {
  'use strict';

  angular
    .module('zenquizz')
    .filter('extension', extension);

  function extension() {
    return extensionFilter;

    function extensionFilter(key) {
      if (angular.isDefined(key)) {
        var index = key.lastIndexOf('.');
        return key.substr(index + 1);
      }
    }
  }
})();
