(function() {
  'use strict';

  describe('Filter categories', function() {


    beforeEach(module('zenquizz'));

    it('should has a categories filter', inject(function($filter){
      expect($filter('categories')).not.toEqual(null);
    }));

    it('should return categories in parentheses', inject(function($filter){
      expect($filter('categories')(['one','two'])).toBe("(one / two)");
    }));
  });
})();
