(function() {
  'use strict';

  describe('Filter AttachedFile', function() {


    beforeEach(module('zenquizz'));


    it('should has a AttachedFile Filter', inject(function($filter){
      expect($filter('attachedFile')).not.toEqual(null);
    }));

    it('should do nothing', inject(function(attachedFileFilter){
      expect(attachedFileFilter("")).toBeDefined();
    }));

    it('should truncate UUID',inject(function(attachedFileFilter){
      var key = "068da87a-2a6f-476a-8cc4-0658d1380624-5DmyxpZn_400x400.jpg";
      expect(attachedFileFilter(key)).toBe("5DmyxpZn_400x400.jpg");
    }));
  });
})();
