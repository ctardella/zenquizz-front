(function() {
  'use strict';

  describe('Filter diacritics', function() {


    beforeEach(module('zenquizz'));


    it('should has a diacritics filter', inject(function($filter){
      expect($filter('diacritics')).not.toEqual(null);
    }));

    it('should remove accent', inject(function(diacriticsFilter){
      expect(diacriticsFilter("éèçàùâäüëïîê")).toBe("eecauaaueiie");
    }));
  });
})();
