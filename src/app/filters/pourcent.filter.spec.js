(function() {
  'use strict';

  describe('Filter pourcent', function() {


    beforeEach(module('zenquizz'));


    it('should has a pourcent filter', inject(function($filter){
      expect($filter('pourcent')).not.toEqual(null);
    }));

    it('should filter as number filter + % char',inject(function(pourcentFilter){
      expect(pourcentFilter(10.2)).toBe("1,020.000%");
      expect(pourcentFilter(115.567)).toBe("11,556.700%");
    }));
  });
})();
