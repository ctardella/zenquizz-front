(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalGitDetailController', ModalGitDetailController);

    /** @ngInject */
    function ModalGitDetailController($uibModalInstance,data) {
      var vm =this;
      vm.data = data;
      vm.ok =  function(){
        $uibModalInstance.close();
      };
    }
})();
