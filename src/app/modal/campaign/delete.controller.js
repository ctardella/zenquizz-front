(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalDeleteCampaignController', ModalDeleteCampaignController);

    /** @ngInject */
    function ModalDeleteCampaignController($uibModalInstance,datas) {
      var vm =this;
      vm.text = datas;
      vm.ok =  function(){
        $uibModalInstance.close();
      };
      vm.cancel = function(){
        $uibModalInstance.dismiss('cancel');
      };
    }
})();
