(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalConfirmController', ModalConfirmController);

    /** @ngInject */
    function ModalConfirmController($uibModalInstance,datas) {
      var vm =this;
      vm.text = datas.text;
      vm.type = datas.type;
      vm.isArray = function(item){
        return angular.isArray(item);
      };
      vm.ok =  function(){
        $uibModalInstance.close();
      };
      vm.cancel = function(){
        $uibModalInstance.dismiss('cancel');
      };
    }
})();
