(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalCategoryController', ModalCategoryController);

    /** @ngInject */
    function ModalCategoryController($uibModalInstance) {
      var vm =this;

      vm.ok =  function(){
        $uibModalInstance.close(vm.newName);
      };
      vm.cancel = function(){
        $uibModalInstance.dismiss('cancel');
      };
    }
})();
