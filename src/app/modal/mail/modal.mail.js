(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalMailController', ModalMailController);

    /** @ngInject */
    function ModalMailController($uibModalInstance) {
      var vm =this;
      vm.ok =  function(){
        $uibModalInstance.close();
      };
      vm.cancel = function(){
        $uibModalInstance.dismiss('cancel');
      };
    }
})();
