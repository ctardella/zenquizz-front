(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalRenameController', ModalConfirmController);

    /** @ngInject */
    function ModalConfirmController($uibModalInstance,datas) {
      var vm =this;
      vm.quizz = datas.quizz;
      vm.submit =  function(){
        $uibModalInstance.close();
      };
      vm.cancel = function(){
        $uibModalInstance.dismiss('cancel');
      };
    }
})();
