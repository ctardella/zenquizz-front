(function() {
  'use strict';

  angular
    .module('zenquizz')
    .controller('ModalPageChangeController', ModalPageChangeController);

    /** @ngInject */
    function ModalPageChangeController($uibModalInstance) {
      var vm =this;
      vm.save=  function(){
        $uibModalInstance.close();
      };
      vm.quit = function(){
        $uibModalInstance.dismiss('quit');
      };
    }
})();
