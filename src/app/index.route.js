(function() {
  'use strict';

  angular
    .module('zenquizz')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/main',
        template: '<loginpage></loginpage>'
      })
      .state('home', {
        abstract:true,
        template: '<home></home>',
        ncyBreadcrumb: {
          skip: true
        }
      })
      .state('answer',{
        url:'/answer/:id',
        template: '<response></reponse>'
      })
      .state('home.main', {
        url: '/',
        template: '<quizz-list></quizz-list><campaign-list></campaign-list><quizz-search></quizz-search>',
        ncyBreadcrumb: {
          label: 'Home'
        }
      })
      .state('answerIndiv',{
        url:'/answer/:campaignId/:recipientId',
        template: '<response-landingpage></response-landingpage>'
      })
      .state('markdown', {
        url: '/markdown',
        template: '<markdown-helper></markdown-helper>',
        ncyBreadcrumb: {
          parent: "home.main",
          label: 'Markdown Helper'
        }
      })
      .state('home.campaigns', {
        url: '/campaigns',
        template: '<campaign-list></campaign-list>',
        ncyBreadcrumb: {
          parent: "home.main",
          label: 'Campagne'
        }
      })
      .state('home.createCampaign', {
        url: '/createCampaign',
        template: '<campaign-create></campaign-create>',
        ncyBreadcrumb: {
          parent: "home.main",
          label: 'Campagne'
        }
      })
      .state('home.campaign', {
        url: '/campaign/:quizzId/:id',
        template:'<campaign></campaign>',
        ncyBreadcrumb: {
          parent: "home.campaigns",
          label: '{{::vm.campaign.name}} {{::vm.campaign.categories | categories}}'
        }
      })
      .state('home.campaignDetail', {
        url: '/detail/campaign/:id',
        template:'<campaign-detail></campaign-detail>',
        ncyBreadcrumb: {
          parent: "home.campaigns",
          label: 'Détail des résultats'
        }
      })
      .state('home.search', {
        url: '/search',
        template: '<quizz-search></quizz-search>',
        ncyBreadcrumb: {
          label: 'Recherche'
        }
      })
      .state('home.quizzes', {
        url: '/quizzes',
        template: '<quizz-list></quizz-list>',
        ncyBreadcrumb: {
          parent:"home.main",
          label: 'Quizz'
        }
      })
      .state('home.quizz', {
        url: '/quizz/:id/:cat',
        template:'<quizz></quizz>',
        ncyBreadcrumb: {
          parent:'home.quizzes',
          label:'{{::vm.quizz.name}} {{::vm.quizz.categories | categories}}'
        }
      })
      .state('quizzPrint',{
        url:'/print/quizz/:quizzId',
        template:'<quizz-print></quizz-print>'
      })
      .state('home.archived', {
        url: '/campaign/archived',
        template:'<campaign-archive></campaign-archive>',
        ncyBreadcrumb: {
          parent:'home.campaign',
          label:'Campagne archivées'
        }
      });
    $urlRouterProvider.otherwise('/');
  }

})();
