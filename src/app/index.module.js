(function() {
  'use strict';

  angular
    .module('zenquizz', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria',
     'ngResource', 'ui.router', 'ui.bootstrap', 'ngTable', 'ui.tree', 'angular.config', 'ncy-angular-breadcrumb',
     'angularSpinner','hc.marked','monospaced.elastic','duScroll','ui.bootstrap.contextMenu','ui.bootstrap.datetimepicker'
     ,'flow','angular-clipboard','toastr','cfp.hotkeys','ngPrettyJson']);

})();
