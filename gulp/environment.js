
var gulp = require('gulp');
var path = require('path');
var conf = require('./conf');
var del = require('del');


var gulpNgConfig = require('gulp-ng-config');

gulp.task('environment:dev', function () {
  //del(path.join(conf.paths.src,'/app/configFile.js'));
  gulp.src(path.join(conf.paths.src, 'configFile.json'))
  .pipe(gulpNgConfig('angular.config',{
    environment:'local'
  }))
  .pipe(gulp.dest(path.join(conf.paths.src,'/app/')));
});

gulp.task('environment:prod', function () {
  //del(path.join(conf.paths.src,'/app/configFile.js'));
  gulp.src(path.join(conf.paths.src, 'configFile.json'))
    .pipe(gulpNgConfig('angular.config',{
      environment:'production'
    }))
    .pipe(gulp.dest(path.join(conf.paths.src,'/app/')));
});
